# API REST server

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-server/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-server/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-server/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-server/commits/test)

Server implementation of the [OpenAPI definition](https://gitlab.com/webengproteam/url-shortener/api-rest-definition).

## Requirements
Python 3.6+

## Authentication

In order to make requests to the endpoints that require authentication, all excepting create_user and login,
it is necesary to send the token received by one of those two methods.

The token must be sent following the [Bearer standard](https://tools.ietf.org/html/rfc6750) as a header parameter in the form `Authentication: Bearer token123`.

## Local development

To install the general dependencies run:

```
pip3 install -r requirements.txt
```

And to install the testing dependencies, run this in addition to the previous one:

```
pip3 install -r test-requirements.txt
```

To set up the database run:

```bash
source set_db.sh
```

Then set the proper environmental variables with:

```bash
source set_env.sh local
```

At this point you can run the tests:

```
nosetests
```

Or run the server:

```
pip3 install -r requirements.txt
python3 -m swagger_server
```

and open your browser to here:

```
http://localhost:8080/v3/ui/
```

Later you can clean the database with:

```bash
source unset_db.sh
```

Take into consideration that you can't use this at the same time that docker-compose, since the
mapped ports are the same.


## Docker-compose

If you just want to run the latest version of the server, you can use docker-compose

```bash
docker-compose pull && docker-compose up
```
