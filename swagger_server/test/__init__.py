import logging

import connexion
from flask_testing import TestCase
from flask import json

from flask_jwt_extended import (JWTManager)

from swagger_server.encoder import JSONEncoder
from swagger_server.db.config import config_db
from swagger_server.config import BaseConfig
from swagger_server.models.user_request import UserRequest
from swagger_server.models.user_login import UserLogin


class BaseTestCase(TestCase):

    def create_app(self):
        logging.getLogger('connexion.operation').setLevel('ERROR')
        app = connexion.App(__name__, specification_dir='../swagger/')
        app.app.json_encoder = JSONEncoder
        app.add_api('swagger.yaml')

        # Config database access
        app.app.config.from_object(BaseConfig)
        config_db(app.app)

        jwt = JWTManager(app.app)

        return app.app


def login_any(self, user_email, user_pass):
    user_request = UserRequest(email=user_email, _pass=user_pass)

    response = self.client.open(
        '/v3/users/login',
        method='POST',
        data=json.dumps(user_request),
        content_type='application/json'
    )

    return UserLogin.from_dict(response.json)


def login_john(self):
    return login_any(
        self,
        user_email='john@mail.com',
        user_pass='john'
    )


def login_mary(self):
    return login_any(
        self,
        user_email='mary@mail.com',
        user_pass='mary'
    )


def set_token_header(token):
    return {'Authorization': f'Bearer {token}'}


def assert_status(self, expected, response):
    if expected == 200:
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
    elif expected == 204:
        self.assertStatus(response, 204,
                          'Response body is : ' + response.data.decode('utf-8'))
    elif expected == 401:
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))
    elif expected == 403:
        self.assert403(response,
                       'Response body is : ' + response.data.decode('utf-8'))
    elif expected == 404:
        self.assert404(response,
                       'Response body is : ' + response.data.decode('utf-8'))

