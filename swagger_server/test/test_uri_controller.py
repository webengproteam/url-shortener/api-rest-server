# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.hook import Hook  # noqa: E501
from swagger_server.models.hook_trigger import HookTrigger  # noqa: E501
from swagger_server.models.hook_action_http import HookActionHTTP  # noqa: E501
from swagger_server.models.hook_action_mail import HookActionMail  # noqa: E501
from swagger_server.models.uri import URI  # noqa: E501
from swagger_server.models.uri_request import URIRequest  # noqa: E501
from swagger_server.test import BaseTestCase

from swagger_server.test import login_john, login_any, assert_status, set_token_header


class TestUriController(BaseTestCase):
    """UriController integration test stubs"""

    def test_activate_uri_200(self):
        """Test case 200 for activate_uri

        Activate a URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/deactivate'.format(uriId='abc123'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(False, URI.from_dict(response.json).activated,
                         'Response body is : ' + response.data.decode('utf-8'))

        response = self.client.open(
            '/v3/uris/{uriId}/activate'.format(uriId='abc123'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(True, URI.from_dict(response.json).activated,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_activate_uri_401(self):
        """Test case 401 for activate_uri

        Activate a URI
        """
        response = self.client.open(
            '/v3/uris/{uriId}/activate'.format(uriId='abc123'),
            method='PUT')

        assert_status(self, 401, response)

    def test_activate_uri_403(self):
        """Test case 403 for activate_uri

        Activate a URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/activate'.format(uriId='abc4'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_activate_uri_404(self):
        """Test case 404 for activate_uri

        Activate a URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/activate'.format(uriId='nope'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )
        assert_status(self, 404, response)

    def test_add_uri_200(self):
        """Test case 200 for add_uri

        Add a new URI
        """

        user_login = login_john(self)

        real_uri = 'https://unizar.es'
        expected_owner = 1

        body = URIRequest(real_uri=real_uri)
        response = self.client.open(
            '/v3/uris',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(real_uri, URI.from_dict(response.json).real_uri,
                         'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(expected_owner, URI.from_dict(response.json).user_id,
                         'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(True, URI.from_dict(response.json).activated,
                         'Response body is : ' + response.data.decode('utf-8'))
        self.assertIsNotNone(URI.from_dict(response.json).short_uri,
                             'Response body is : ' + response.data.decode('utf-8'))

    def test_add_uri_400(self):
        """Test case 400 for add_uri

        Add a new URI
        """

        user_login = login_john(self)

        real_uri = 'httpsss://unizar.es'

        body = URIRequest(real_uri=real_uri)
        response = self.client.open(
            '/v3/uris',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 400, response)

        real_uri = 'unizar.es'

        body = URIRequest(real_uri=real_uri)
        response = self.client.open(
            '/v3/uris',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 400, response)

    def test_add_uri_401(self):
        """Test case 401 for add_uri

        Add a new URI
        """

        real_uri = 'https://unizar.es'

        body = URIRequest(real_uri=real_uri)

        response = self.client.open(
            '/v3/uris',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')

        assert_status(self, 401, response)

    def test_deactivate_uri_401(self):
        """Test case 401 for deactivate_uri

        Deactivate a URI
        """
        response = self.client.open(
            '/v3/uris/{uriId}/deactivate'.format(uriId='abc123'),
            method='PUT')

        assert_status(self, 401, response)

    def test_deactivate_uri_403(self):
        """Test case 403 for deactivate_uri

        Deactivate a URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/deactivate'.format(uriId='abc4'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_deactivate_uri_404(self):
        """Test case 404 for deactivate_uri

        Deactivate a URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/deactivate'.format(uriId='nope'),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_delete_uri_204(self):
        """Test case 204 for delete_uri

        Delete URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}/uris'.format(userId=1),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        uri = [URI.from_dict(uri) for uri in response.json][-1]

        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId=uri.short_uri),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 204, response)

    def test_delete_uri_401(self):
        """Test case 401 for delete_uri

        Delete URI
        """
        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId='abc6'),
            method='DELETE')

        assert_status(self, 401, response)

    def test_delete_uri_403(self):
        """Test case 403 for delete_uri

        Delete URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId='abc6'),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_delete_uri_404(self):
        """Test case 404 for delete_uri

        Delete URI
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId='nope'),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_get_hooks_by_uri_200_list(self):
        """Test case 200-list for get_hooks_by_uri

        Retrieves all the hooks of a URI
        """
        uri_id = 'abc123'
        expected_hook_list = [
            Hook(
                hook_id=1,
                user_id=1,
                uri_id='abc123',
                trigger=HookTrigger(
                    clicks=5,
                    minutes=120
                ),
                actions_http=[
                    HookActionHTTP(
                        endpoint='http://myendpoint.org/here',
                        verb='GET',
                        params='?query=lol',
                        body={"thing": {"hi": 3}}
                    )
                ],
                actions_mail=[
                    HookActionMail(
                        destinations=['myfriend@mail.com'],
                        subject='Hey!',
                        text='Lorem ipsum'
                    )
                ],
                active=True
            )
        ]
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/hooks'.format(uriId=uri_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)

        received_hook_list = [Hook.from_dict(hook) for hook in response.json]
        self.assertEqual(expected_hook_list[0], received_hook_list[0],
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_hooks_by_uri_200_empty(self):
        """Test case 200-empty for get_hooks_by_uri

        Retrieves all the hooks of a URI
        """
        uri_id = 'abc4'
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/hooks'.format(uriId=uri_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(0, response.json.__len__(),
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_hooks_by_uri_404(self):
        """Test case 404 for get_hooks_by_uri

        Retrieves all the hooks of a URI
        """
        uri_id = '0'
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/hooks'.format(uriId=uri_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_get_uri_200(self):
        """Test casec 200 for get_uri

        Find URI by ID
        """
        short_uri = 'abc123'
        expected_real_uri = 'https://twitter.com'
        expected_owner = 1
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId=short_uri),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(expected_real_uri, URI.from_dict(response.json).real_uri,
                         'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(expected_owner, URI.from_dict(response.json).user_id,
                         'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(True, URI.from_dict(response.json).activated,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_uri_404(self):
        """Test casec 404 for get_uri

        Find URI by ID
        """
        short_uri = '0'
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}'.format(uriId=short_uri),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)


if __name__ == '__main__':
    import unittest
    unittest.main()
