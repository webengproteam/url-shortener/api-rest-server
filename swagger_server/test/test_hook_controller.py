# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.hook import Hook  # noqa: E501
from swagger_server.models.hook_request import HookRequest  # noqa: E501
from swagger_server.models.hook_trigger import HookTrigger
from swagger_server.models.hook_action_http import HookActionHTTP
from swagger_server.models.hook_action_mail import HookActionMail
from swagger_server.test import BaseTestCase

from swagger_server.test import login_john, login_mary, assert_status, set_token_header



class TestHookController(BaseTestCase):
    """HookController integration test stubs"""
    
    def test_activate_hook_200(self):
        """Test case 200 for activate_hook

        Activate a Hook
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}/deactivate'.format(hookId=1),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(False, Hook.from_dict(response.json).active,
                         'Response body is : ' + response.data.decode('utf-8'))

        response = self.client.open(
            '/v3/hooks/{hookId}/activate'.format(hookId=1),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(True, Hook.from_dict(response.json).active,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_activate_hook_401(self):
        """Test case 401 for activate_hook

        Activate a Hook
        """
        response = self.client.open(
            '/v3/hooks/{hookId}/activate'.format(hookId=1),
            method='PUT')
        self.assert401(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_activate_hook_403(self):
        """Test case 403 for activate_hook

        Activate a Hook
        """
        user_login = login_mary(self)

        response = self.client.open(
            '/v3/hooks/{hookId}/activate'.format(hookId=1),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_activate_hook_404(self):
        """Test case 404 for activate_hook

        Activate a Hook
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}/activate'.format(hookId=789),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 4004, response)

    def test_create_hook_200(self):
        """Test case 200 for create_hook

        Create hook
        """
        body = HookRequest(
                uri_id='abc123',
                trigger=HookTrigger(
                    clicks=5,
                    minutes=120
                ),
                actions_http=[
                    HookActionHTTP(
                        endpoint='http://api.thing.com/do',
                        verb='POST',
                        params='?thing',
                        body={"thing": {"hi": 3}}
                    )
                ],
                actions_mail=[
                    HookActionMail(
                        destinations=['myfriend@mail.com'],
                        subject='Hey!',
                        text='Lorem ipsum'
                    )
                ],
            )

        expected_hook = Hook(
                hook_id=None,
                user_id=1,
                uri_id='abc123',
                trigger=HookTrigger(
                    clicks=5,
                    minutes=120
                ),
                actions_http=[
                    HookActionHTTP(
                        endpoint='http://api.thing.com/do',
                        verb='POST',
                        params='?thing',
                        body={"thing": {"hi": 3}}
                    )
                ],
                actions_mail=[
                    HookActionMail(
                        destinations=['myfriend@mail.com'],
                        subject='Hey!',
                        text='Lorem ipsum'
                    )
                ],
                active=True
            )

        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)

        received_hook = Hook.from_dict(response.json)
        received_hook.hook_id = None # We can't predict which id it'll have
        self.assertEqual(expected_hook, received_hook,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_create_hook_401(self):
        """Test case 401 for create_hook

        Create hook
        """
        body = HookRequest()

        response = self.client.open(
            '/v3/hooks',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')

        assert_status(self, 401, response)

    def test_deactivate_hook_401(self):
        """Test case 401 for deactivate_hook

        Deactivate a Hook
        """
        response = self.client.open(
            '/v3/hooks/{hookId}/deactivate'.format(hookId=1),
            method='PUT')

        assert_status(self, 401, response)

    def test_deactivate_hook_403(self):
        """Test case 403 for deactivate_hook

        Deactivate a Hook
        """
        user_login = login_mary(self)

        response = self.client.open(
            '/v3/hooks/{hookId}/deactivate'.format(hookId=1),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_deactivate_hook_404(self):
        """Test case 404 for deactivate_hook

        Deactivate a Hook
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}/deactivate'.format(hookId=789),
            method='PUT',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)
        
    def test_delete_hook_204(self):
        """Test case 204 for delete_hook

        Delete Hook
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/uris/{uriId}/hooks'.format(uriId='abc123'),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        hook = [Hook.from_dict(hook) for hook in response.json][-1]

        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=hook.hook_id),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 204, response)

    def test_delete_hook_401(self):
        """Test case 401 for delete_hook

        Delete Hook
        """
        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=1),
            method='DELETE')

        assert_status(self, 401, response)

    def test_delete_hook_403(self):
        """Test case 403 for delete_hook

        Delete Hook
        """
        user_login = login_mary(self)

        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=1),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_delete_hook_404(self):
        """Test case 404 for delete_hook

        Delete Hook
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=0),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_get_hook_200(self):
        """Test case 200 for get_hook

        Retrieve a hook
        """
        hook_id = 1
        expected_hook = Hook(
            hook_id=1,
            user_id=1,
            uri_id='abc123',
            trigger=HookTrigger(
                clicks=5,
                minutes=120
            ),
            actions_http=[
                HookActionHTTP(
                    endpoint='http://myendpoint.org/here',
                    verb='GET',
                    params='?query=lol',
                    body={"thing": {"hi": 3}}
                )
            ],
            actions_mail=[
                HookActionMail(
                    destinations=['myfriend@mail.com'],
                    subject='Hey!',
                    text='Lorem ipsum'
                )
            ],
            active=True
        )
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=hook_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        received_hook = Hook.from_dict(response.json)
        assert_status(self, 200, response)
        self.assertEqual(expected_hook, received_hook,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_hook_404(self):
        """Test case 404 for get_hook

        Retrieve a hook
        """
        hook_id = 0
        user_login = login_john(self)

        response = self.client.open(
            '/v3/hooks/{hookId}'.format(hookId=hook_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)


if __name__ == '__main__':
    import unittest
    unittest.main()
