# coding: utf-8

from __future__ import absolute_import

from flask import json

from swagger_server.models.uri import URI  # noqa: E501
from swagger_server.models.user import User  # noqa: E501
from swagger_server.models.user_request import UserRequest  # noqa: E501
from swagger_server.models.user_login import UserLogin
from swagger_server.test import BaseTestCase

from swagger_server.test import login_john, login_any, assert_status, set_token_header


class TestUserController(BaseTestCase):
    """UserController integration test stubs"""

    def test_create_user(self):
        """Test case for create_user

        Create user
        """
        user_mail = 'test@mail.com'
        user_passwd = 'test'
        body = UserRequest(email=user_mail, _pass=user_passwd)
        expected_user = User(email=user_mail)

        response = self.client.open(
            '/v3/users',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')

        assert_status(self, 200, response)
        user_login = UserLogin.from_dict(response.json)
        self.assertEqual(expected_user.email, user_login.user.email,
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_user_204(self):
        """Test case 204 for delete_user

        Delete user
        """
        user_email = 'test@mail.com'
        user_pass = 'test'
        user_login = login_any(self, user_email, user_pass)
        user_id = user_login.user.user_id

        response = self.client.open(
            '/v3/users/{userId}'.format(userId=user_id),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 204, response)

    def test_delete_user_401(self):
        """Test case 401 for delete_user

        Delete user
        """
        response = self.client.open(
            '/v3/users/{userId}'.format(userId=1),
            method='DELETE')

        assert_status(self, 401, response)

    def test_delete_user_403(self):
        """Test case 403 for delete_user

        Delete user
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}'.format(userId=2),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 403, response)

    def test_delete_user_404(self):
        """Test case 404 for delete_user

        Delete user
        """
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}'.format(userId=0),
            method='DELETE',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_get_uri_by_user_200(self):
        """Test case 200 for get_uri_by_user

        Retrieves all the URI of a user
        """
        user_id = 1
        expected_uri_list = [
            URI(
                uri_id='abc123',
                real_uri='https://twitter.com',
                short_uri='abc123',
                activated=True,
                reachable=False,
                safe=False,
                user_id=1
            )
        ]

        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}/uris'.format(userId=user_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(expected_uri_list[0], [URI.from_dict(uri) for uri in response.json][0],
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_uri_by_user_404(self):
        """Test case 404 for get_uri_by_user

        Retrieves all the URI of a user
        """
        user_id = 0
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}/uris'.format(userId=user_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 404, response)

    def test_get_user_by_id_200(self):
        """Test case 200 for get_user_by_id

        Get user by user id
        """
        user_id = 1
        user_email = 'john@mail.com'
        expected_user = User(user_id=user_id, email=user_email)
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}'.format(userId=user_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )

        assert_status(self, 200, response)
        self.assertEqual(expected_user, User.from_dict(response.json),
                         'Response body is : ' + response.data.decode('utf-8'))

    def test_get_user_by_id_404(self):
        """Test case 404 for get_user_by_id

        Get user by user id
        """
        user_id = 0
        user_login = login_john(self)

        response = self.client.open(
            '/v3/users/{userId}'.format(userId=user_id),
            method='GET',
            headers=set_token_header(user_login.token)
        )
        assert_status(self, 404, response)

    def test_login_user_200(self):
        """Test case 200 for login_user

        Logs user into the system
        """

        user_email = 'john@mail.com'
        user_pass = 'john'
        user_request = UserRequest(email=user_email, _pass=user_pass)

        response = self.client.open(
            '/v3/users/login',
            method='POST',
            data=json.dumps(user_request),
            content_type='application/json'
        )

        assert_status(self, 200, response)

    def test_login_user_401(self):
        """Test case 401 for login_user

        Logs user into the system
        """

        user_email = 'john@mail.com'
        user_pass = 'wrongpass'
        user_request = UserRequest(email=user_email, _pass=user_pass)

        response = self.client.open(
            '/v3/users/login',
            method='POST',
            data=json.dumps(user_request),
            content_type='application/json'
        )

        assert_status(self, 401, response)


if __name__ == '__main__':
    import unittest
    unittest.main()
