from hashlib import sha256
import random
import string
import json

from swagger_server.models.user import User as API_User
from swagger_server.models.uri import URI as API_Uri
from swagger_server.models.hook import Hook as API_Hook
from swagger_server.models.hook_trigger import HookTrigger as API_HookTrigger
from swagger_server.models.hook_action_http import HookActionHTTP as API_HookActionHTTP
from swagger_server.models.hook_action_mail import HookActionMail as API_HookActionMail

from swagger_server.db.config import db


class Users(db.Model):
    __table_name__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Text, unique=True, nullable=False)
    passwd_hash = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return f'<User {self.email}>'

    def __init__(self, api_user_request):
        self.email = api_user_request.email
        self.passwd_hash = sha256(api_user_request._pass.encode('utf8')).hexdigest().upper()

    def to_api(self):
        return API_User(
            user_id=self.id,
            email=self.email
        )


class Uri(db.Model):
    __table_name__ = 'uri'

    short_uri = db.Column(db.String(20), primary_key=True)
    long_uri = db.Column(db.String(100), nullable=False)
    accessible = db.Column(db.Boolean, nullable=False)
    active = db.Column(db.Boolean, nullable=False)
    secure = db.Column(db.Boolean, nullable=False)
    admin = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('Users', backref=db.backref('uris', lazy=True, cascade='all'))

    short_uri_length = 8

    def __repr__(self):
        return f'<URI {self.short_uri}>'

    def __init__(self, api_uri_request, user_id):
        self.long_uri = api_uri_request.real_uri
        self.short_uri = ''.join(
            [random.choice(string.ascii_letters + string.digits) for n in range(self.short_uri_length)]
        )
        self.accessible = False
        self.active = True
        self.secure = False
        self.admin = user_id

    def to_api(self):
        return API_Uri(
            uri_id=self.short_uri,
            short_uri=self.short_uri,
            real_uri=self.long_uri,
            user_id=self.admin,
            activated=self.active,
            reachable=self.accessible,
            safe=self.secure
        )


class HookTrigger(db.Model):
    __table_name__ = 'hook_trigger'

    id = db.Column(db.Integer, primary_key=True)
    clicks = db.Column(db.Integer)
    minutes = db.Column(db.Integer)
    admin = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('Users', backref=db.backref('hooks', lazy=True, cascade='all'))
    uri = db.Column(db.String(20), db.ForeignKey('uri.short_uri'), nullable=False)
    uri_ref = db.relationship('Uri', backref=db.backref('hooks', lazy=True, cascade='all'))
    active = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return f'<HookTrigger {self.id}>'

    def __init__(self, api_hook_request, user_id):
        self.uri = api_hook_request.uri_id
        self.clicks = api_hook_request.trigger.clicks
        self.minutes = api_hook_request.trigger.minutes
        self.admin = user_id
        self.active = True

        for action in api_hook_request.actions_http:
            self.actions_http.append(HookActionHttp(api_hook_action_http=action))

        for action in api_hook_request.actions_mail:
            self.actions_email.append(HookActionEmail(api_hook_action_email=action))

    def to_api(self):
        return API_Hook(
            hook_id=self.id,
            uri_id=self.uri,
            user_id=self.admin,
            active=self.active,
            trigger=API_HookTrigger(
                clicks=self.clicks,
                minutes=self.minutes
            ),
            actions_http=[action.to_api() for action in self.actions_http],
            actions_mail=[action.to_api() for action in self.actions_email]
        )


class HookActionHttp(db.Model):
    __table_name__ = 'hook_action_http'

    id = db.Column(db.Integer, primary_key=True)
    http_endp = db.Column(db.Text)
    http_verb = db.Column(db.Text)
    http_params = db.Column(db.Text)
    http_body = db.Column(db.Text)
    hook_id = db.Column(db.Integer, db.ForeignKey('hook_trigger.id'), nullable=False)
    user = db.relationship('HookTrigger', backref=db.backref('actions_http', lazy=True, cascade='all'))

    def __repr__(self):
        return f'<HookActionHTTP {self.id}>'

    def __init__(self, api_hook_action_http):
        self.http_endp = api_hook_action_http.endpoint
        self.http_verb = api_hook_action_http.verb
        self.http_params = api_hook_action_http.params
        self.http_body = json.dumps(api_hook_action_http.body)

    def to_api(self):
        return API_HookActionHTTP(
            endpoint=self.http_endp,
            verb=self.http_verb,
            params=self.http_params,
            body=json.loads(self.http_body)
        )


class HookActionEmail(db.Model):
    __table_name__ = 'hook_action_email'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Text)
    subject = db.Column(db.Text)
    body = db.Column(db.Text)
    hook_id = db.Column(db.Integer, db.ForeignKey('hook_trigger.id'), nullable=False)
    user = db.relationship('HookTrigger', backref=db.backref('actions_email', lazy=True, cascade='all'))

    def __repr__(self):
        return f'<HookActionEmail {self.id}>'

    def __init__(self, api_hook_action_email):
        self.address = ','.join(api_hook_action_email.destinations)
        self.subject = api_hook_action_email.subject
        self.body = api_hook_action_email.text

    def to_api(self):
        return API_HookActionMail(
            destinations=self.address.split(','),
            subject=self.subject,
            text=self.body,
        )
