# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.hook_trigger import HookTrigger
from swagger_server.models.hook_action_http import HookActionHTTP
from swagger_server.models.hook_action_mail import HookActionMail
from swagger_server import util


class Hook(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(self, hook_id: int=None, user_id: int=None, uri_id: str=None, active: bool=None, trigger: HookTrigger=None, actions_http: List[HookActionHTTP]=None, actions_mail: List[HookActionMail]=None):  # noqa: E501
        """Hook - a model defined in Swagger

        :param hook_id: The hook_id of this Hook.  # noqa: E501
        :type hook_id: int
        :param user_id: The user_id of this Hook.  # noqa: E501
        :type user_id: int
        :param uri_id: The uri_id of this Hook.  # noqa: E501
        :type uri_id: str
        :param active: The active of this Hook.  # noqa: E501
        :type active: bool
        :param trigger: The trigger of this Hook.  # noqa: E501
        :type trigger: HookTrigger
        :param actions_http: The actions_http of this Hook.  # noqa: E501
        :type actions_http: List[HookActionHTTP]
        :param actions_mail: The actions_mail of this Hook.  # noqa: E501
        :type actions_mail: List[HookActionMail]
        """
        self.swagger_types = {
            'hook_id': int,
            'user_id': int,
            'uri_id': str,
            'active': bool,
            'trigger': HookTrigger,
            'actions_http': List[HookActionHTTP],
            'actions_mail': List[HookActionMail]
        }

        self.attribute_map = {
            'hook_id': 'hookId',
            'user_id': 'userId',
            'uri_id': 'uriId',
            'active': 'active',
            'trigger': 'trigger',
            'actions_http': 'actionsHTTP',
            'actions_mail': 'actionsMail'
        }

        self._hook_id = hook_id
        self._user_id = user_id
        self._uri_id = uri_id
        self._active = active
        self._trigger = trigger
        self._actions_http = actions_http
        self._actions_mail = actions_mail

    @classmethod
    def from_dict(cls, dikt) -> 'Hook':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Hook of this Hook.  # noqa: E501
        :rtype: Hook
        """
        return util.deserialize_model(dikt, cls)

    @property
    def hook_id(self) -> int:
        """Gets the hook_id of this Hook.


        :return: The hook_id of this Hook.
        :rtype: int
        """
        return self._hook_id

    @hook_id.setter
    def hook_id(self, hook_id: int):
        """Sets the hook_id of this Hook.


        :param hook_id: The hook_id of this Hook.
        :type hook_id: int
        """

        self._hook_id = hook_id

    @property
    def user_id(self) -> int:
        """Gets the user_id of this Hook.


        :return: The user_id of this Hook.
        :rtype: int
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id: int):
        """Sets the user_id of this Hook.


        :param user_id: The user_id of this Hook.
        :type user_id: int
        """

        self._user_id = user_id

    @property
    def uri_id(self) -> str:
        """Gets the uri_id of this Hook.


        :return: The uri_id of this Hook.
        :rtype: str
        """
        return self._uri_id

    @uri_id.setter
    def uri_id(self, uri_id: str):
        """Sets the uri_id of this Hook.


        :param uri_id: The uri_id of this Hook.
        :type uri_id: str
        """

        self._uri_id = uri_id

    @property
    def active(self) -> bool:
        """Gets the active of this Hook.


        :return: The active of this Hook.
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active: bool):
        """Sets the active of this Hook.


        :param active: The active of this Hook.
        :type active: bool
        """

        self._active = active

    @property
    def trigger(self) -> HookTrigger:
        """Gets the trigger of this Hook.


        :return: The trigger of this Hook.
        :rtype: HookTrigger
        """
        return self._trigger

    @trigger.setter
    def trigger(self, trigger: HookTrigger):
        """Sets the trigger of this Hook.


        :param trigger: The trigger of this Hook.
        :type trigger: HookTrigger
        """

        self._trigger = trigger

    @property
    def actions_http(self) -> List[HookActionHTTP]:
        """Gets the actions_http of this Hook.


        :return: The actions_http of this Hook.
        :rtype: List[HookActionHTTP]
        """
        return self._actions_http

    @actions_http.setter
    def actions_http(self, actions_http: List[HookActionHTTP]):
        """Sets the actions_http of this Hook.


        :param actions_http: The actions_http of this Hook.
        :type actions_http: List[HookActionHTTP]
        """

        self._actions_http = actions_http

    @property
    def actions_mail(self) -> List[HookActionMail]:
        """Gets the actions_mail of this Hook.


        :return: The actions_mail of this Hook.
        :rtype: List[HookActionMail]
        """
        return self._actions_mail

    @actions_mail.setter
    def actions_mail(self, actions_mail: List[HookActionMail]):
        """Sets the actions_mail of this Hook.


        :param actions_mail: The actions_mail of this Hook.
        :type actions_mail: List[HookActionMail]
        """

        self._actions_mail = actions_mail
