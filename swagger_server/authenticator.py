from flask_jwt_extended import (
    create_access_token,
    decode_token
)
from flask import g


def check_bearerAuth(token):
    decoded = decode_token(token)
    if decoded:
        # g is a global object that only lives during the current request
        g.identity = decoded['identity']

    # decoded is returned because otherway connexion crashes
    return decoded


def sign_in(userid):
    return create_access_token(userid)


def get_userid():
    return g.identity


def enforce_auth(func):
    def inner(*args, **kwargs):
        # The connexion library adds two params that we don't need.
        # These params come from the dict returned at check_bearerAuth
        kwargs.pop('user', None)
        kwargs.pop('token_info', None)

        if not g.identity:
            return 'Need authentication', 401
        return func(*args, **kwargs)
    return inner


def enforce_user(userid):
    def wrapper(func):
        def inner(*args, **kwargs):
            if g.identity != userid:
                return 'Forbidden for this user', 403
            return func(*args, **kwargs)
        return inner
    return wrapper
