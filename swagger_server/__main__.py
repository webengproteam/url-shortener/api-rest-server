#!/usr/bin/env python3

import connexion
from flask_jwt_extended import (JWTManager)
from flask_cors import CORS

from swagger_server import encoder
from swagger_server.db.config import config_db
from swagger_server.config import BaseConfig


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'URL Shortener API'})

    # Config database access
    app.app.config.from_object(BaseConfig)
    config_db(app.app)
    CORS(app.app, supports_credentials=True)
    jwt = JWTManager(app.app)

    app.run(port=8080)


if __name__ == '__main__':
    main()
