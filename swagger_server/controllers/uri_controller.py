import connexion
import six
import re

from swagger_server.models.uri_request import URIRequest as API_URIRequest  # noqa: E501
from swagger_server import util

from swagger_server.db.models import Uri as DB_Uri
from swagger_server.db.models import HookTrigger as DB_Hook

import swagger_server.authenticator as auth
from swagger_server.db.config import db

import random
import string

@auth.enforce_auth
def activate_uri(uriId, body):  # noqa: E501
    """Activate a URI

     # noqa: E501

    :param uriId: ID of URI to activate
    :type uriId: str

    :rtype: URI
    """
    uri = DB_Uri.query.filter_by(short_uri=uriId).first()
    if not uri:
        return 'Not found', 404

    @auth.enforce_user(uri.admin)
    def f():
        uri.active = True
        db.session.commit()

        return uri.to_api()

    return f()

http_pattern = re.compile('https?://')


@auth.enforce_auth
def add_uri(body):  # noqa: E501
    """Add a new URI

     # noqa: E501

    :param body: URI that needs to be created
    :type body: dict | bytes

    :rtype: URI
    """
    if connexion.request.is_json:
        body = API_URIRequest.from_dict(connexion.request.get_json())  # noqa: E501

    if not http_pattern.match(body.real_uri):
        return 'Invalid URI', 400

    uri = DB_Uri(api_uri_request=body, user_id=auth.get_userid())

    db.session.add(uri)
    db.session.commit()

    return uri.to_api()


@auth.enforce_auth
def deactivate_uri(uriId, body):  # noqa: E501
    """Deactivate a URI

     # noqa: E501

    :param uriId: ID of URI to deactivate
    :type uriId: str

    :rtype: URI
    """
    uri = DB_Uri.query.filter_by(short_uri=uriId).first()
    if not uri:
        return 'Not found', 404

    @auth.enforce_user(uri.admin)
    def f():
        uri.active = False
        db.session.commit()

        return uri.to_api()

    return f()


@auth.enforce_auth
def delete_uri(uriId):  # noqa: E501
    """Deletes a URI

     # noqa: E501

    :param uriId: URI id to delete
    :type uriId: str

    :rtype: None
    """
    uri = DB_Uri.query.filter_by(short_uri=uriId).first()
    if not uri:
        return 'Not found', 404

    @auth.enforce_user(uri.admin)
    def f():
        db.session.delete(uri)
        db.session.commit()

        return 'Removed', 204

    return f()


def get_hooks_by_uri(uriId):  # noqa: E501
    """Retrieves all the hooks of a URI

     # noqa: E501

    :param uriId: ID of URI to get hooks from
    :type uriId: str

    :rtype: List[Hook]
    """

    uri = DB_Uri.query.filter_by(short_uri=uriId).first()
    if not uri:
        return 'Not found', 404

    return [hook.to_api() for hook in uri.hooks]


def get_uri(uriId):  # noqa: E501
    """Find URI by ID

     # noqa: E501

    :param uriId: ID of URI to return
    :type uriId: str

    :rtype: URI
    """

    uri = DB_Uri.query.filter_by(short_uri=uriId).first()
    if not uri:
        return 'Not found', 404

    return uri.to_api()
