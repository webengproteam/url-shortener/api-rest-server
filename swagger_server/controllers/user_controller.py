import connexion
import six

from swagger_server.models.uri import URI as API_Uri  # noqa: E501
from swagger_server.models.user import User as API_User # noqa: E501
from swagger_server.models.user_request import UserRequest as API_UserRequest # noqa: E501
from swagger_server.models.user_login import UserLogin as API_UserLogin # noqa: E501
from swagger_server import util

from swagger_server.db.config import db
from swagger_server.db.models import Users as DB_User
from swagger_server.db.models import Uri as DB_Uri

import swagger_server.authenticator as auth

from hashlib import sha256


def create_user(body):  # noqa: E501
    """Create user

    This can only be done by the logged in user. # noqa: E501

    :param body: Created user object
    :type body: dict | bytes

    :rtype: User
    """
    if connexion.request.is_json:
        body = API_UserRequest.from_dict(connexion.request.get_json())  # noqa: E501

    new_user = DB_User(api_user_request=body)
    db.session.add(new_user)
    db.session.commit()

    return API_UserLogin(
        token=auth.sign_in(new_user.id),
        user=new_user.to_api()
    )


@auth.enforce_auth
def delete_user(userId):  # noqa: E501
    """Delete user

    This can only be done by the logged in user. # noqa: E501

    :param userId: The id that needs to be deleted
    :type userId: int

    :rtype: None
    """

    user = DB_User.query.filter_by(id=userId).first()
    if not user:
        return 'Not found', 404

    @auth.enforce_user(userId)
    def f():
        db.session.delete(user)
        db.session.commit()

        return 'Removed', 204

    return f()


def get_uri_by_user(userId):  # noqa: E501
    """Retrieves all the URI of a user

     # noqa: E501

    :param userId: ID of user to get URI from
    :type userId: int

    :rtype: List[URI]
    """

    user = DB_User.query.filter_by(id=userId).first()
    if not user:
        return 'Not found', 404

    uris = user.uris
    uri_list = [uri.to_api() for uri in uris]

    return uri_list


def get_user_by_id(userId):  # noqa: E501
    """Get user by user id

     # noqa: E501

    :param userId: The id that needs to be fetched.
    :type userId: int

    :rtype: User
    """

    user = DB_User.query.filter_by(id=userId).first()
    if not user:
        return 'Not found', 404

    return user.to_api()


def login_user(body):  # noqa: E501
    """Logs user into the system

     # noqa: E501

    :param body: Authentication values
    :type body: dict | bytes

    :rtype: UserLogin
    """
    if connexion.request.is_json:
        body = API_UserRequest.from_dict(connexion.request.get_json())  # noqa: E501

    pw_hash = sha256(body._pass.encode('utf8')).hexdigest().upper()
    user = DB_User.query.filter_by(email=body.email, passwd_hash=pw_hash).first()
    if not user:
        return 'Unauthorized', 401

    return API_UserLogin(
        token=auth.sign_in(user.id),
        user=user.to_api()
    )
