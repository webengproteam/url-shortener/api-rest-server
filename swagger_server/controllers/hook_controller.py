import connexion
import six

from swagger_server.models.hook_request import HookRequest as API_HookRequest  # noqa: E501
from swagger_server.db.models import HookTrigger as DB_Hook
from swagger_server import util

from swagger_server.db.config import db
import swagger_server.authenticator as auth


@auth.enforce_auth
def activate_hook(hookId, body):  # noqa: E501
    """Activate a hook

     # noqa: E501

    :param hookId: ID of hook to activate
    :type hookId: int

    :rtype: Hook
    """
    hook = DB_Hook.query.filter_by(id=hookId).first()
    if not hook:
        return 'Not found', 404

    @auth.enforce_user(hook.admin)
    def f():
        hook.active = True
        db.session.commit()

        return hook.to_api()

    return f()


@auth.enforce_auth
def create_hook(body):  # noqa: E501
    """Create hook

     # noqa: E501

    :param body: Created hook object
    :type body: dict | bytes

    :rtype: Hook
    """
    if connexion.request.is_json:
        body = API_HookRequest.from_dict(connexion.request.get_json())  # noqa: E501

    hook = DB_Hook(api_hook_request=body, user_id=auth.get_userid())
    db.session.add(hook)
    db.session.commit()

    return hook.to_api()


@auth.enforce_auth
def deactivate_hook(hookId, body):  # noqa: E501
    """Deactivate a hook

     # noqa: E501

    :param hookId: ID of hook to deactivate
    :type hookId: int

    :rtype: Hook
    """
    hook = DB_Hook.query.filter_by(id=hookId).first()
    if not hook:
        return 'Not found', 404

    @auth.enforce_user(hook.admin)
    def f():
        hook.active = False
        db.session.commit()

        return hook.to_api()

    return f()


@auth.enforce_auth
def delete_hook(hookId):  # noqa: E501
    """Deletes a hook

     # noqa: E501

    :param hookId: Hook id to delete
    :type hookId: int

    :rtype: None
    """
    hook = DB_Hook.query.filter_by(id=hookId).first()
    if not hook:
        return 'Not found', 404

    @auth.enforce_user(hook.admin)
    def f():
        db.session.delete(hook)
        db.session.commit()

        return 'Removed', 204

    return f()


def get_hook(hookId):  # noqa: E501
    """Retrieve a hook

     # noqa: E501

    :param hookId: ID of Hook to retrieve
    :type hookId: int

    :rtype: Hook
    """

    hook = DB_Hook.query.filter_by(id=hookId).first()
    if not hook:
        return 'Not found', 404

    return hook.to_api()


def update_hook(hookId, body):  # noqa: E501
    """Update hook

     # noqa: E501

    :param hookId: ID of Hook to retrieve
    :type hookId: int
    :param body: Created hook object
    :type body: dict | bytes

    :rtype: Hook
    """
    if connexion.request.is_json:
        body = API_HookRequest.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
